package entity;

public class AddOperation implements Operation {

    @Override
    public double execute(double amount, double operand) {
        return amount + operand;
    }
}
