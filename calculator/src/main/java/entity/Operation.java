package entity;

public interface Operation {
    double execute(double amount, double operand);
}
