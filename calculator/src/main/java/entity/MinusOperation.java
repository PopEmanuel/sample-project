package entity;

public class MinusOperation implements Operation {

    @Override
    public double execute(double amount, double operand) {
        return amount - operand;
    }
}
