package entity;

public class MultiplyOperation implements Operation {

    @Override
    public double execute(double amount, double operand) {
        return amount * operand;
    }
}
