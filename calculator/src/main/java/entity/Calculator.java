package entity;

public class Calculator {
    double amount;

    public Calculator() {
        amount = 0;
    }

    public void performOperation(Operation operation, double operand) {
        this.amount = operation.execute(amount, operand);
    }

    public void reset() {
        amount = 0;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
