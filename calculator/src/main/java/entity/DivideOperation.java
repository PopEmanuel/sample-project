package entity;

public class DivideOperation implements Operation {

    @Override
    public double execute(double amount, double operand) {
        if (operand != 0)
            return amount / operand;
        else {
            throw new ArithmeticException("Division by zero is not possible");
        }
    }
}
