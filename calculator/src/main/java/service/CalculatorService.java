package service;

import entity.*;

public class CalculatorService {
    private final Calculator calculator;

    public CalculatorService(Calculator calculator) {
        this.calculator = calculator;
    }

    public double getCalculatorAmount() {
        return calculator.getAmount();
    }

    public void performOperation(char operator, double number) {
        switch (operator) {
            case '+' -> calculator.performOperation(new AddOperation(), number);
            case '-' -> calculator.performOperation(new MinusOperation(), number);
            case '*' -> calculator.performOperation(new MultiplyOperation(), number);
            case '/' -> calculator.performOperation(new DivideOperation(), number);
        }
    }

    public void reset() {
        calculator.reset();
    }

    public void setCalculatorAmount(double value) {
        calculator.setAmount(value);
    }
}
