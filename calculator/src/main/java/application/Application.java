package application;

import entity.Calculator;
import service.CalculatorService;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    private final CalculatorService service;

    public Application() {
        Calculator calculator = new Calculator();
        this.service = new CalculatorService(calculator);
    }

    private static void printMenu() {
        System.out.println("Choose operation : \n");
        System.out.println("+   -   /   *   c(reset)    0(exit)");
    }

    public void startApplication() {
        System.out.println("Welcome to calculator :");

        while (true) {
            try {
                printCalculatorAmount();
                printMenu();
                char operator = takeOperator();
                if (!interpretOperator(operator)) {
                    return;
                }

            } catch (Exception e) {
                System.out.println("Invalid input");
            }

        }
    }

    private boolean interpretOperator(char operator) throws IOException {
        if (operator == '0')
            return false;
        else if (operator == 'c')
            service.reset();
        else {
            System.out.println("Choose amount : ");
            double amount = takeAmount();
            try{
                service.performOperation(operator, amount);
            }catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
        return true;
    }

    private char takeOperator() throws IOException {
        Scanner scanner = new Scanner(System.in);
        return scanner.next().charAt(0);
    }

    private double takeAmount() throws IOException {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    private void printCalculatorAmount() {
        System.out.println("Calculator amount : " + service.getCalculatorAmount());
        System.out.println("\n");
    }

}
