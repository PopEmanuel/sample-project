package service;

import entity.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

public class CalculatorServiceTest {
    CalculatorService calculatorService;

    @Before
    public void setUp() {
        Calculator calculator = new Calculator();
        calculator.setAmount(100);
        calculatorService = new CalculatorService(calculator);
    }

    @After
    public void after() {

    }

    @Test
    public void testGetCalculatorAmount() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.setCalculatorAmount(112.0);
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(112.0));
    }

    @Test
    public void testPerformAddOperation() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.performOperation('+', 10);
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(110.0));
    }

    @Test
    public void testPerformMinusOperation() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.performOperation('-', 10);
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(90.0));
    }

    @Test
    public void testPerformDivideOperation() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.performOperation('/', 10);
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(10.0));
    }

    @Test
    public void testPerformMultiplyOperation() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.performOperation('*', 10);
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(1000.0));
    }

    @Test
    public void testReset() {
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(100.0));
        calculatorService.reset();
        MatcherAssert.assertThat(calculatorService.getCalculatorAmount(), equalTo(0.0));
    }
}